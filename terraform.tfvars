/**
 * Copyright 2021 Mantel Group Pty Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

# region = "us-central1"
# zone = "us-central1-c"
# project_id = "{{ GCP_PROJECT }}"
# gitlab_url = "https://gitlab.com/"
# ci_token = "{{ CI_TOKEN }}"
# credentials_file = "{{ SA }}"
# ci_worker_service_account = "{{ GCP_CI }}"

# ci_concurrency      = 5
# ci_worker_idle_time = 3600
