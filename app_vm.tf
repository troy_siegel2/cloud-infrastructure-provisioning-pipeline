/* data "template_file" "app" {
  templatefile = file("scripts/app.sh")
  vars = {
    shell_user = "dtu_training"
  }
} */

/* templatefile("${path.module}/scripts/app.sh", {
  shell_user = "dtu_training"
}) */

/* resource "google_compute_network" "vpc_network" {
  name                    = "terraform-network"
  auto_create_subnetworks = "true"
} */

resource "google_compute_instance" "app_vm_instance" {
  name                    = "app-${count.index}"
  metadata_startup_script = "templatefile(“${path.module}/scripts/app.sh”, { shell_user = dtu_training })"
  machine_type            = "f1-micro"
  zone                    = var.gcp_zone
  count = 2
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2204-lts"
    }
  }
  network_interface {
    network = "terraform-network"
    subnetwork = "terraform-network"
    access_config {
      // Ephemeral public IP
      network_tier = "STANDARD"
    }
  }
}

/* resource "google_compute_firewall" "allow-ssh-rdp-icmp" {
  name    = "allow-tcp22-tcp3389-icmp"
  network = google_compute_network.vpc_network.name

  allow {
    protocol = "tcp"
    ports    = ["22", "3389", "443"]
  }

  allow {
    protocol = "icmp"
  }
  source_ranges = ["0.0.0.0/0"]
}
 */

