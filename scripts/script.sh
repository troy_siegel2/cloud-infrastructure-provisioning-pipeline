#!/bin/sh

GCLOUD_LOCATION=$(command -v gcloud)
echo "Using gcloud from $GCLOUD_LOCATION"

gcloud --version
#echo "running gcloud services $1 appengine.googleapis.com --project $2"
#gcloud services "$1" appengine.googleapis.com --project "$2"

sudo apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \

# Get the Docker repo GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Add the Docker Ubuntu repo
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get -y update

# Add the Gitlab repo
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

# Install Docker CE and the Gitlab runner
sudo apt-get -y install docker-ce docker-ce-cli containerd.io gitlab-runner


ENV=$ENV
TOKEN=$TOKEN

echo "waiting 180 seconds for docker install"

timeout 180 /bin/bash -c

sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "${TOKEN}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --docker-privileged \
  --description "${ENV}-gitlab-runner" \
  --tag-list "${ENV},all, pipeline, runner, linux, gcp, terraform" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"
